import java.util.ArrayList;

public class Functions {
    static int partition(int[] array, int low, int high){
        int pivot = array[high];
        int i = (low - 1);

        for(int j=low; j <= high -1; j++){
            //If current element is smaller than the pivot
            if(array[j] < pivot){
                i++;
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
        int temp = array[i+1];
        array[i+1] = array[high];
        array[high] = temp;
        return (i+1);
    }

    static int[] quickSort(int[] array,int low,int high){
        if(low < high){
            int pi = partition(array, low, high);

            quickSort(array, low, pi-1);
            quickSort(array, pi +1, high);
        }
        return array;
    }

    //Credits to https://www.geeksforgeeks.org/quick-sort/

    //Credits to https://www.tutorialspoint.com/data_structures_algorithms/shell_sort_algorithm.htm

    static int[] shellSort(int[] array){
        int gap = 1;
        //Work out the interval
        while(gap < array.length/3){
            gap = gap*3 + 1;
        }

        while(gap > 0){
            for(int i = gap; i<array.length; i++){
                //Selects a value to be inserted
                int num = array[i];
                int j=i;

                //Shifts elements right
                while((j > gap -1) && array[j-gap] >= num){
                    array[j] = array[j-gap];
                    j -= gap;
                }

                //Inserts the number at hole position
                array[j] = num;
            }
            gap = (gap -1)/3;
        }
        return array;
    }

    static boolean isExtreme(int num, int [] a){
        for(int i=1; i< a.length -1; i++){
            if(a[i] == num){

                if((a[i-1] < a[i] &&  a[i] > a[i + 1])||(a[i-1] > a[i] && a[i]< a[i+1])){
                    return true;
                }
            }
        }
        return false;
    }

    static ArrayList<Pair> pairs(int [] a){
        ArrayList<Pair> pairs = new ArrayList<Pair>();

        for(int i = 0; i< a.length-1; i++){
            for(int j = i+1; j<a.length; j++){
                if(a[i] != a[j]){
                    Pair p = new Pair(a[i],a[j]);
                    pairs.add(p);
                }
            }
        }
        return pairs;
    }

    static long factorial(int n){
        if(n == 0 || n == 1){
            return 1;
        }else{
            return n*factorial(n-1);
        }
    }

    //https://www.javatpoint.com/prime-number-program-in-java
    static boolean isPrime(int num){
        int m = num/2;
        if(num == 0 || num == 1){
            return false;
        }else{
            for(int i = 2; i<m;i++){
                if(num%i == 0){
                    return false;
                }
            }
        }
        return true;
    }

    static boolean sieveOfEratosthenes(int max){
        if(max <= 1){
            System.out.println("No prime numbers");
            return false;
        }

        boolean[] prime = new boolean[max+1];
        for(int i=0;i<max;i++) {
            prime[i] = true;
        }
        for(int p = 2; p*p <= max; p++){
            //If prime[p] is not changed, it is a prime number
            if(prime[p]){
                //updating all multiples of p to false
                for(int i = p*2; i<=max; i+=p){
                    prime[i] = false;
                }
            }
        }

        //printing all the prime numbers
        for(int i = 2; i <= max; i++){
            if(prime[i]) {
                System.out.print(i + " ");
            }
        }
        return true;
    }
}