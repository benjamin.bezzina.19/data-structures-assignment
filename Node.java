public class Node {
    private int data;
    private Node left;
    private Node right;
    public Node(int d){
        data = d;
        left = right = null;
    }
    //Getter and setter methods
    public int getData(){
        return data;
    }
    public Node getLeft(){
        return left;
    }
    public void setLeft(Node l){
        left = l;
    }
    public Node getRight(){
        return right;
    }
    public void setRight(Node r){
        right = r;
    }
}
