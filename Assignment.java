import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.Stack;


//java.util.Collections;
public class Assignment {
    public static void main(String[] args) {
        int[] a = new int[256];
        int[] b = new int[512];
        question1(a,b);
        question2(a,b);
        question3(new int[] {9,1,7,2,3,0,14,22});

        int[] array = new int[10];
        for(int num : array){
            num = (int)(Math.random() * ((1024 - 1) + 1)) + 1;
            System.out.println(num);
        }
        question4(array);
        
        System.out.println(question5("37+64-/"));
        question6(2,10);
        question7();
        System.out.println(question8(4));
        question9(new int[] {4,1,3,4,1});
        System.out.println(question10(new int[] {-5,1,3,45,1}));
        System.out.println(question11(Math.PI,10));
        System.out.println(question12(10));
    }
     

    static void question1(int[]A, int[]B) {

        // Assigns a random int value between 0 and 1024 for each integer in A
        for (int i = 0; i < A.length; i++) {
            A[i] = (int) (Math.random() * 1024);
        }
        // Assigns a random int value between 0 and 1024 for each integer in B
        for (int i = 0; i < B.length; i++) {
            B[i] = (int) (Math.random() * 1024);
        }

        //Displays A before sorting
        System.out.println("A before sorting:");
        for(int i : A){
            System.out.print(i + " ");
        }
        System.out.println("");
        A = Functions.shellSort(A);

        System.out.println("A After sorting:");
        for(int i : A){
            System.out.print(i + " ");
        }
        System.out.println("");
        System.out.println("Array B before sorting:");
        //Displays B Before Sorting
        for(int i : B){
            System.out.print(i + " ");
        }
        System.out.println("");

        B = Functions.quickSort(B,0,B.length -1);
        System.out.println("Array B after sorting:");
        for(int i: B){
            System.out.print(i + " ");
        }
        System.out.println("");
    }
    //https://www.geeksforgeeks.org/merge-two-sorted-arrays/

    static void question2(int[] a, int[] b ){
        int[] c = new int[a.length + b.length];

        //Copying all elements in A into C
        for(int i= 0; i<a.length; i++){
            c[i] = a[i];
        }
        int i=0,j=0,k=0;

          /* Check if current element of first
             array is smaller than current element
             of second array. If yes, store first
             array element and increment first array
             index. Otherwise do same with second array*/

        while(i<a.length && j<b.length ){
            if(a[i] < b[j]){
                c[k] = a[i];
                k++;
                i++;
            }else{
                c[k] = b[j];
                k++;
                j++;
            }
        }
        for(;i<a.length;i++){
            c[k] = a[i];
            k++;
        }

        for(;j<b.length;j++){
            c[k] = b[j];
            k++;
        }
        System.out.println("Array C");
        //Displaying array C
        for(int item : c){
            System.out.println(item);
        }

    }


    static void question3(int [] a){
        ArrayList <Integer> extremeNums = new ArrayList<Integer>();
        for(int i = 0; i<a.length; i++){
            if(Functions.isExtreme(a[i], a)){
                extremeNums.add(a[i]);
            }
        }
        if(extremeNums.isEmpty()){
            System.out.println("Sorted");
        }else{
            for(int i = 0; i<extremeNums.size(); i++){
                System.out.println(extremeNums.get(i));
            }
        }
        /*Yes, an value is extreme only if the values in the array are not in ascending order
        If an array is sorted, all the values are then in ascending order meaning that no value
        is extreme*/
    }

    static void question4(int[] a){
        ArrayList<Pair> allPairs = Functions.pairs(a);
        ArrayList<Pair> twoPairs = new ArrayList<Pair>();
        for(int i = 0; i<allPairs.size();i++){
            for(int j = 0;j<allPairs.size(); j++){
                if((allPairs.get(i) != allPairs.get(j)) && (allPairs.get(i).num1 * allPairs.get(i).num2 == allPairs.get(j).num1 * allPairs.get(j).num2)){
                    twoPairs.add(allPairs.get(i));
                    twoPairs.add(allPairs.get(j));
                }
            }
        }

        for(int i = 0; i< twoPairs.size(); i++){
            System.out.println(twoPairs.get(i).num1 + " x " + twoPairs.get(i).num2 + " = " + twoPairs.get(i+1).num1 + " x " + twoPairs.get(i+1).num2);
            i++;
        }
        if(twoPairs.size() == 0){
            System.out.println("No 2-Pairs");
        }
    }

    static int question5(String expression){
        Stack<Integer> stack = new Stack();
        int num1= 0;
        int num2 = 0;

        for(int i = 0;i<expression.length();i++){
            if(Character.isDigit(expression.charAt(i))){
                stack.push(Character.getNumericValue(expression.charAt(i)));
            }else{
                try {
                    switch (expression.charAt(i)) {
                        case '+':
                            num1 = stack.pop();
                            num2 = stack.pop();
                            stack.push(num1 + num2);
                            break;
                        case '-':
                            num1 = stack.pop();
                            num2 = stack.pop();
                            stack.push(num2 - num1);
                            break;
                        case '*':
                            num1 = stack.pop();
                            num2 = stack.pop();
                            stack.push(num1 * num2);
                            break;
                        case '/':
                            num1 = stack.pop();
                            num2 = stack.pop();
                            stack.push(num2 / num1);
                            break;

                        default:
                            System.out.println("Invalid operation");
                            System.exit(1);

                    }
                }catch (EmptyStackException e){
                    System.out.println("Invalid expression");
                    System.exit(1);
                }
            }
        }
        return stack.pop();
    }

    static void question6(int checkIfPrime, int sieveOfEratosthenes){
        System.out.println("Checking if " + checkIfPrime + " is a prime number");
        if(Functions.isPrime(checkIfPrime)){
            System.out.println(checkIfPrime + " Is a prime number");
        }else{
            System.out.println(checkIfPrime + " Is not a prime number");
        }

        System.out.println("Displaying all the prime numbers from 0 to " + sieveOfEratosthenes);
        Functions.sieveOfEratosthenes(sieveOfEratosthenes);
    }

    static void question7(){
        //Reference Charles Axisa A level notes 2018
        /*Due to the question asking the sequence of integers to be read 1 by 1
        I understood that the question requires promting the user to add an integer each time*/

        BinaryTree bst = new BinaryTree();
        Scanner k = new Scanner(System.in);

        System.out.println("Enter an integer");
        int number = k.nextInt();
        Node root = new Node(number);

        System.out.println("How many more integers do want to add?");
        int max = k.nextInt();
        System.out.println("Enter each Integer one by one");
        for(int i = 0;i<max;i++){
            //creates a node taking the entered integer as data and adds it to the tree
            bst.addNode(new Node(k.nextInt()),root);
            bst.displayTree(root,5);
            System.out.println("Tree with added node");
        }
    }

    static double question8(double num){
        /*As a starting approximation for the newton-raphson method of
        approximating the square root, below it takes the number that we want
        to find the square root of as our first guess. Although this is not
        the ideal guess, it can be used as a starting point to go closer and closer
        to the actual value*/

        double guess = num;
        double difference = num;
        //The accuracy of the approximation is set to +/- 0.00000001
        final double ERROR = 0.00000001;

        while(difference > ERROR ) {
            //The derivitave of f(x)^2 is 2f(x)

            double newGuess = guess - (Math.pow(guess,2) - num) / (2*guess);
            difference = Math.abs(newGuess - guess);
            guess = newGuess;
        }
        return guess;
    }

    static ArrayList<Integer> question9(int [] list){
        //This algorithm only works for integers between 0 and n-1
        //https://www.geeksforgeeks.org/find-duplicates-in-on-time-and-constant-extra-space/
        ArrayList <Integer> duplicates = new ArrayList<Integer>();
        try {
            for (int i = 0; i < list.length; i++) {
                if (list[Math.abs(list[i])] >= 0) {
                    list[Math.abs(list[i])] *= -1;
                } else {
                    if (list[i] >= 0) {
                        duplicates.add(list[i]);
                    } else {
                        duplicates.add(-list[i]);
                    }
                }
            }
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("List of size n must only contain integers in the range 0 to (n-1)");
            System.exit(1);
        }
        //Displaying all duplicates
        if(duplicates.size() == 0){
            System.out.println("No duplicates found");
            return duplicates;
        }

        for(int i = 0; i<duplicates.size();i++){
            System.out.println(duplicates.get(i));
        }

        return duplicates;
    }
    static int question10(int[] list){
        // https://stackoverflow.com/questions/12711397/python-recursive-function-to-find-the-largest-number-in-the-list
        if(list.length <= 1){
            return list[0];
        }else{
            int [] shortList = new int[list.length-1];
            for(int i = 1; i<list.length;i++){
                shortList[i-1] = list[i];
            }
            int max = question10(shortList);
            if(max > list[0]){
                return max;
            }else{
                return list[0];
            }
        }
    }

    static double question11(double num, int n) {
        /*This function approximates the value of sin(num)
        by taking the first n terms of the series expansion of sin
        */
        Scanner k = new Scanner(System.in);
        String choice;
        boolean validChoice = false;
        do {
            System.out.println("Do you want to calculate the sin or cos of " + num);
            choice = k.nextLine();
            if (choice.equalsIgnoreCase("sin")) {
                double sin = 0;
                for (int i = 0; i < n; i++) {
                    sin += ((Math.pow(-1, i) / Functions.factorial((2 * i) + 1)) * Math.pow(num, (2 * i) + 1));
                }
                validChoice = true;
                if(Double.isNaN(sin)){
                    System.out.println("Error");
                    System.exit(1);
                }
                return sin;
            } else if (choice.equalsIgnoreCase("cos")) {
                double cos = 0;
                for (int i = 0; i < n; i++) {
                    cos += ((Math.pow(-1, i) / Functions.factorial(2 * i)) * Math.pow(num, (2 * i)));
                }
                validChoice = true;
                if(Double.isNaN(cos)){
                    System.out.println("Error");
                    System.exit(1);
                }
                return cos;
            } else {
                System.out.println("Invalid choice");
            }
        }while(!validChoice);
        //The below return value should never be reached
        return 0;
    }

    static long question12(int n){
        if(n <= 1){
            return n;
        }else{
            return question12(n-1) + question12(n-2);
        }
    }

}




