public class BinaryTree{
    private Node root;
    public BinaryTree(){
        root = null;
    }

    //Recursive method to add a node to the bst
    public void addNode(Node n, Node r){
        if(r == null){
            root = n;
        }else if(n.getData() < r.getData()){
            if(r.getLeft() == null){
                r.setLeft(n);
            }else{
                addNode(n,r.getLeft());
            }
        }else if(n.getData() > r.getData()){
            if(r.getRight() == null){
                r.setRight(n);
            }else{
                addNode(n, r.getRight());
            }
        }
    }
    //Display Tree
    //https://www.geeksforgeeks.org/print-binary-tree-2-dimensions/
     void displayTree(Node root, int space){
        final int COUNT = 10;
        // Base case
        if (root == null)
            return;

        // Increase distance between levels
        space += COUNT;

        // Process right child first
        displayTree(root.getRight(), space);

        // Print current node after space
        // count
        System.out.print("\n");
        for (int i = COUNT; i < space; i++)
            System.out.print(" ");
        System.out.print(root.getData() + "\n");

        // Process left child
        displayTree(root.getLeft(), space);
    }
}
